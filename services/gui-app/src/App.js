import React from 'react';
import './App.css';

const LoadState = Object.freeze({
    NOT_LOADED:   Symbol("NOT_LOADED"),
    LOADING:  Symbol("LOADING"),
    LOADED: Symbol("LOADED")
});

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            loadState: LoadState.NOT_LOADED,
            items: []
        };
        console.log('constructor')
        this.boundClick = this.loadData.bind(this)
    }

    loadData(e) {
        this.setState({
            loadState: LoadState.LOADING,
            items: []
        })
        fetch("http://35.232.207.169/get-data?key=1234")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        loadState: LoadState.LOADED,
                        items: result.items
                    });
                },
                // Note: it's important to handle errors here
                // instead of a catch() block so that we don't swallow
                // exceptions from actual bugs in components.
                (error) => {
                    this.setState({
                        loadState: LoadState.NOT_LOADED,
                        error
                    });
                }
            )
    }

    render() {
        const {error, loadState, items} = this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (loadState === LoadState.LOADING) {
            return <div>Loading...</div>;
        } else {
            return (
                <div>
                    <button onClick={this.boundClick }>Load some data</button>
                <ul>
                    {items.map(item => (
                        <li key={item.name}>
                            {item.name} {item.price}
                        </li>
                    ))}
                </ul>
                </div>
            );

        }
    }
}

export default App;
