# Lab 3 - Performance Testing

In this lab, we will design some performance tests that can run as part of our pipeline to test capacity in our system

When you are building a product you normally have an intended goal in mind. You might have 15 users, you might have 15 million. The decisions you make in your system design should be influenced by the realities of how it is being used.

Often, The first step in this process is describing the load of your system. Load can be described the term `load parameters`  The best parameters depends on the system itself. Some examples may be
* requests per seconds for a web server 
* writes/sec for a database
* the time it takes for an update to flow through your whole system. 

Once you have identified your `load parameters` you  have a KPI you can monitor. Only then can you start considering scalability and reliability. What happens if the load on my web server doubles? What happens if our system has to suddenly ingest five times as many messages?


We can describe the performance of our system against two contrasting view points. Some sample questions have been added against these 2 primary load investigations for an example web service like the read through view-cache service

1. When we increase the load parameter and keep system resources the same (CPU, memory, bandwidth etc) unchanged, how is the performance of your system affected? 
 - How is response latency affected (mean, 95th and 99th)
 - Are the primitive resources we have (CPU, Memory, network and file IO) drastically spiking compared to the relationship of the increase in load.
2. When we increase the load parameter, how much do you need to increase the resources of your system to keep performance unchanged? 
 - Does our system allow for easy scaling and sharding, do we have certain bottlenecks in our design
 - Does our system have the capability to scale up and down to keep costs under control


## Part 1 - Testing the View Cache with Gatling

Errata: We use gatling for this lab. There are numerous load testing tools out there. We also execute the the load test in a single container running on the same cluster. We advise against this. Your load test is likely being constrained by the network and CPU throughput of the single container. Running your load test in parallel is relatively easy and is described here. Alternatively you can use  BIG FAT INSTANCE with high CPU and Network IO to execute the test. We would also recommend that you deploy your load tests onto a different cluster to provide isolation and accuracy. 

### Structure of Gatling Tests

Gatling is a JVM based load test which uses a DSL which your write in Scala to define your test. A simple load test looks like this
```scala

package loadtests

import io.gatling.core.Predef._
import io.gatling.core.scenario.Simulation
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._
import scala.concurrent.duration._

class ViewCacheServiceSimulation extends Simulation {
  val url = "http://35.184.138.147/get-data?key=1234" //terrible terrible hardcoding this
  val scn: ScenarioBuilder = scenario("view-cache-simulation")
    .exec(http("view-cache-simulation")
      .get(url)
      .check(status is 200)
    )

  setUp(
    scn.inject(
      rampUsersPerSec (0) to 425 during (10 second),
      constantUsersPerSec(425) during (5 minutes)
    )
  )
  .assertions(forAll.successfulRequests.percent.gte(99.9))
  .assertions(forAll.requestsPerSec.between(395, 500))
}


```

We are calling the same URL 425 times per second (immediately after 1 second) and then running that constantly for 5 minutes. We are ensuring that 99.9 percent of our requests returns a HTPP 200 status. We are also ensuring that we are continuously being able to serve between 395 and 500 requests per second.


This test has a problem. It uses 1 url. Our percentage of cache hits, therefore, should be close to 100%. We need a distribution of requests that is more likely to simulate 'wild' traffic.


We can do this by supplying a function for our load test query string.

```scala

 val randomNumGen = new Random()
 val scn: ScenarioBuilder = scenario("view-cache-simulation")
    .exec(http("view-cache-simulation")
      .get(url)
      .queryParam("key", _ => randomNumGen.nextInt(500).toString))
      .check(status is 200)
    )

```

### Task 1 - Execute the performance tests

You should be able to exec you performance tests by running

`gradlew gatlingRun-loadtests.ViewCacheServiceSimulation`

Note: you will need the [JDK](https://openjdk.java.net/install/) installed on your machine.

After you run the script, you will see the in the output

```
view-cache-simulation: percentage of successful requests is greater than or equal to 99.9 : false
view-cache-simulation: mean requests per second is between inclusive 395.0 and 500.0 : false
```
So our tests are failing. Examining the code, reveals we have some issues to fix. Your task is to fix the performance test such that

* It uses your cluster i.p address instead of `0.0.0.0`
* It uses a generated key for each request between 0-500
* it runs for 10 seconds during the ramp up period and 1 minute at full volume
* It ramps up to 100 users

When run with the correct conditions, you should see the below output 

```
view-cache-simulation: percentage of successful requests is greater than or equal to 99.9 : true
view-cache-simulation: mean requests per second is between inclusive 85.0 and 500.0 : true

BUILD SUCCESSFUL in 1m 20s
```

### Task 2. Having the test run as part of your deployment pipeline

Next, we will create a stage and job to run our performance tests after a deployment. In our gitlab file make the following changes

* Add a stage 'performance' that executes post-deploy
* Create a new job that executes in the performance stage
* This job should use the 'gradle' image
* it should run `gradle gatlingRun-loadtests.ViewCacheServiceSimulation` in the `services/performance` directory

## Part 2 - Visualizing the results with Grafana and Kiali

Navigate to grafana ( :15031 ) on your cluster, and then to the Istio Service Dashboard. Notice there are drop-down selects for each of the services you have deployed. Lets have a look at the view-cache service.

![alt text](nodeconf-istio-graphs.png "sample chart") 

When your performance test is running, notice you are receiving real-time metrics from istio giving you visibility on on the service is performing

Next lets look at kiali ( :15029 )

Go to the Graph section, set the options 
* 'Graph Label -> Requests per second'
* 'Display' add Traffic Animations

You should see a graph which looks similar to the below

![alt text](nodeconf-kiali.png "sample chart")  

You will notice a service flow is displayed in real-time giving an overview over requests transferring between the system. Try playing with the key space to see how it affects throughput to the backend service. 

## Concluding Lab 3

You are now finished with Lab 3 in this lab we learned
* How to construct a simple load test in gatling
* How to execute this load test as part of your gitlab pipeline


In the next lab, we will look at [Monitoring Live Services](../lab-4/README.md)
